package nsu.fabricy.threadpool;

public interface TaskListener {
        void taskInterrupted();
        void taskFinished();
        void taskStarted();
}
