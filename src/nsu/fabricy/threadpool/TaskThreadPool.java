package nsu.fabricy.threadpool;

public class TaskThreadPool {
    private ThreadPool threadPool;
     private Task task;
    public TaskThreadPool(ThreadPool threadPool, Task task){
        this.task = task;
        this.threadPool = threadPool;
    }
    public void start(){
        threadPool.notifySub();
        try {
            task.performedTask();
        }
        catch (InterruptedException e){ }
    }

}
