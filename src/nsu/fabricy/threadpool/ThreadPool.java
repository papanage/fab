package nsu.fabricy.threadpool;

import nsu.fabricy.factory.PsModel.Publisher;

import java.util.*;

public class ThreadPool extends Publisher {
    public Integer countDoneTask = 0;
    public int threadCount;
    private final List<TaskThreadPool> taskQueue = new LinkedList<>();
    private Set availableThreads = new HashSet();
    public void addTask(Task t){
        synchronized (taskQueue){
            System.out.println("нужна машина");
            taskQueue.add(new TaskThreadPool(this, t));
            taskQueue.notify();
            notifySub();
        }
    }
    public ThreadPool(int trC){
        threadCount = trC;
        for (int i=0; i<threadCount; i++)
        {
            availableThreads.add(new Worker(taskQueue, i, false));
        }
        for (Iterator iter = availableThreads.iterator(); iter.hasNext(); )
        {
            ((Thread)iter.next()).start();
        }
    }
    synchronized public Integer getQueueSize(){
        return taskQueue.size();
    }
    public void countDoneInc(){
        countDoneTask++;
        notifySub();
    }

    public void dispose(){
        for (Iterator iter = availableThreads.iterator(); iter.hasNext(); )
        {
            ((Thread)iter.next()).interrupt();
        }
    }
}
