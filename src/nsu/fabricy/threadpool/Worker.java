package nsu.fabricy.threadpool;

import nsu.fabricy.factory.TaskCar;

import java.util.List;

public class Worker extends  Thread{
    private int id;
    private boolean isEndDay;
    private TaskThreadPool task;
    private final List<TaskThreadPool> taskQueue;
    public  Worker(List<TaskThreadPool> taskQueue, int id, boolean isEndDay){
        this.taskQueue = taskQueue;
        this.id = id;
        this.isEndDay = isEndDay;
        System.out.println("Пришел рабочий номер " + id);
    }
    @Override
    public void run() {
        while (true) {
            boolean isGet = false;
            while (!isGet) {
                synchronized (taskQueue) {
                    if (taskQueue.isEmpty()) {
                        try {
                            System.out.println("Пока работы нет " + id);
                            taskQueue.wait();
                            System.out.println("Бегу за заданиями " + id);
                        } catch (InterruptedException e) {

                        }
                    }
                    if (!taskQueue.isEmpty()) {
                        isGet = true;
                        task = taskQueue.remove(0);
                        System.out.println("I got work " + id);
                    }

                }

            }
            try {
                performedTask();
            } catch (InterruptedException e) { }
        }
    }
    public void performedTask() throws InterruptedException{
       task.start();
    }
}
