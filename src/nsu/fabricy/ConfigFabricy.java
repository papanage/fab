package nsu.fabricy;

import java.io.*;
import java.util.List;

public  class ConfigFabricy {
    private static int dealerNum = 2;
    private static int workerNum = 2;
    private static int maxStorageBodySize = 6;
    private static int maxStorageEngineSize = 6;
    private static int maxStorageSuplierSize = 6;
    private static int maxStorageReadySize = 6;
    public static int getDealerNum(){return dealerNum;}
    public static int getWorkerNum(){return workerNum;}
    public static int getMaxStorageBodySize(){return maxStorageBodySize;}
    public static int getMaxStorageEngineSize(){return maxStorageEngineSize;}
    public static int getMaxStorageSuplierSize(){return maxStorageSuplierSize;}
    public static int getMaxStorageReadySize(){return maxStorageReadySize;}
    public ConfigFabricy(){
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File("C:\\fabricy\\src\\nsu\\fabricy\\factory\\resourse\\conf.txt")));
            while((line = reader.readLine()) != null){
                String[] list = line.split(" ");
                switch (list[0]){
                    case "NDR": dealerNum = Integer.parseInt(list[2]);
                        break;
                    case "NWR": workerNum = Integer.parseInt(list[2]);
                        break;
                    case "SBODY": maxStorageBodySize = Integer.parseInt(list[2]);
                        break;
                    case "SENG": maxStorageEngineSize= Integer.parseInt(list[2]);
                        break;
                    case "SSUP": maxStorageSuplierSize = Integer.parseInt(list[2]);
                        break;
                    case "SREAD": maxStorageReadySize = Integer.parseInt(list[2]);
                        break;

                }
            }

        }
        catch (FileNotFoundException e){
            System.err.println("not file");
        }
        catch (IOException e){
            System.err.println("error");
        }

    }
}
