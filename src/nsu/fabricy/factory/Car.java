package nsu.fabricy.factory;

import nsu.fabricy.factory.frames.Body;
import nsu.fabricy.factory.frames.Engine;
import nsu.fabricy.factory.frames.HaveId;
import nsu.fabricy.factory.frames.Suplier;

public class Car extends HaveId {
    private Body body;
    private Engine engine;
    private Suplier suplier;
    public Car(Body body, Engine engine, Suplier suplier){
        super();
        this.body = body;
        this.engine = engine;
        this.suplier = suplier;
    }
    public String CarLog(){
        return ("Auto<"+getid()+"> (Body<"+ body.getid()+ ">, Engine <"+engine.getid()+">, Suplier<"+suplier.getid()+">)");
    }

}
