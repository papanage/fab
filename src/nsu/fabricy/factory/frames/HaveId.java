package nsu.fabricy.factory.frames;

public abstract class HaveId {
    private int id;
    static private int id_counter = 0;
    public HaveId(){
        id = id_counter;
        id_counter++;
    }
    public int  getid(){return id;}
}
