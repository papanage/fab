package nsu.fabricy.factory;

import nsu.fabricy.ConfigFabricy;
import nsu.fabricy.factory.Controllers.StoreReadyController;
import nsu.fabricy.factory.Gui.Gui;
import nsu.fabricy.factory.Stores.Store;
import nsu.fabricy.factory.frames.Body;
import nsu.fabricy.factory.frames.Engine;
import nsu.fabricy.factory.frames.Suplier;
import nsu.fabricy.factory.produsers.Producer;
import nsu.fabricy.threadpool.ThreadPool;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Plant {
    public Store<Body> bodyStore ;
    public Store<Engine> engineStore ;
    public Store<Suplier> suplierStore ;
    private Store<Car> storeReady ;
    private Producer<Body> bodyProducer;
    private Producer<Engine> engineProducer;
    private List<Producer<Suplier>> suplierProducers = new LinkedList<>();
    private List<Dealer> dealers = new LinkedList<>();
    private StoreReadyController  storeReadyController;
    private ThreadPool tp;
    public Integer getTPSize(){
        return tp.getQueueSize();
    }
    public Integer getCountCars(){
        return tp.countDoneTask;
    }
    public enum pr{
        BODYP,
        ENGP,
        SUPP,
        DEALC
    }
    private Logger logger = Logger.getLogger(Plant.class.getName());
    public Integer getCountGeneral(pr a){
        Integer res = 0;
        switch(a){
            case  BODYP:
                res = bodyProducer.getCount();
                break;
            case ENGP:
                res = engineProducer.getCount();
                break;
            case SUPP:
                for(Producer<Suplier> p: suplierProducers ){
                    res += p.getCount();
                }
                break;
        }
        return res;
    }
    public void changeTime(pr a, int time){
       switch (a){
           case  BODYP:
                bodyProducer.changeTime(time);
                break;
           case ENGP:
               engineProducer.changeTime(time);
               break;
           case SUPP:
               for(Producer<Suplier> p: suplierProducers ){
                   p.changeTime(time);
               }
               break;
           case DEALC:
               for(Dealer d: dealers ){
                   d.changeTime(time);
               }


       }
    }
    public Plant(){
        ConfigFabricy configFabricy = new ConfigFabricy();
        bodyStore = new Store<>(ConfigFabricy.getMaxStorageBodySize());
        engineStore = new Store<>(ConfigFabricy.getMaxStorageEngineSize());
        suplierStore = new Store<>(ConfigFabricy.getMaxStorageSuplierSize());
        storeReady = new Store<>(ConfigFabricy.getMaxStorageReadySize());
        bodyProducer = new Producer<>(bodyStore, Body.class);
        engineProducer = new Producer<>(engineStore, Engine.class);
        tp = new ThreadPool(ConfigFabricy.getWorkerNum());
        FileHandler fh;
        try {
            fh = new FileHandler("C:\\fabricy\\src\\nsu\\fabricy\\log.txt");
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.setUseParentHandlers(false);
            logger.addHandler(fh);
        }
        catch (IOException e){}
        engineProducer.start();
        bodyProducer.start();
        for (int i = 0; i < ConfigFabricy.getDealerNum() + 3; i++) {
            suplierProducers.add(new Producer<>(suplierStore, Suplier.class));
            suplierProducers.get(i).start();
        }

        storeReadyController = new StoreReadyController(storeReady, tp, bodyStore, engineStore, suplierStore, logger);

        Gui gui = new Gui( tp,this);

        storeReadyController.start();


        for (int i = 0; i < ConfigFabricy.getDealerNum(); i++){
            dealers.add(new Dealer(tp, i, storeReadyController));
            dealers.get(i).start();
        }

    }
    public void closeFabricy(){
        bodyProducer.interrupt();
        engineProducer.interrupt();
        for (Producer<Suplier> p: suplierProducers){
            p.interrupt();
        }
        storeReadyController.interrupt();
        tp.dispose();
    }
}
