package nsu.fabricy.factory.Stores;

import java.util.LinkedList;
import java.util.List;

public  class  Store<T> {
    protected int maxSize;
    protected int realSize;
    public final   List<T> frameList;
    public int getMaxSize(){return maxSize;}
    public int getRealSizeSize(){return realSize;}
    public Store(int maxSize){
        frameList = new LinkedList<>();
        this.maxSize = maxSize;
        realSize = 0;
    }
   public int put(T frame){
        boolean isPut = false;
        while(!isPut) {
            synchronized (frameList) {
                if (realSize == maxSize) {
                    try {
                        frameList.wait();
                    } catch (InterruptedException e) {
                    }
                }
                if (realSize < maxSize) {
                    isPut = true;
                    System.out.println("Положили деталь " + frame + " " + maxSize + " , " + realSize);
                    frameList.add(frame);
                    realSize++;
                    frameList.notify();
                }
            }
        }
        return 1;
   }

   public T get(){
        while(true) {
            synchronized (frameList) {
                if (realSize == 0) {
                    try {
                        frameList.wait();
                    } catch (InterruptedException e) {
                    }
                }
                if (realSize != 0) {
                    realSize--;
                    T f = frameList.remove(0);
                    frameList.notify();
                    return f;

                }
            }
        }
   }

    public int getRealSize() {
        return realSize;
    }
}
