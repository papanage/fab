package nsu.fabricy.factory.PsModel;

public abstract class Sub implements SubI{
    Publisher publisher;
    public Sub(Publisher publisher){
        this.publisher = publisher;
        publisher.addSub(this);
    }
}
