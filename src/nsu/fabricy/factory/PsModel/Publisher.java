package nsu.fabricy.factory.PsModel;

import java.util.LinkedList;
import java.util.List;

public abstract class Publisher {
    List<Sub> subs = new LinkedList<>();
    public void addSub(Sub sub){
        subs.add(sub);
    }
    public void notifySub(){
        subs.get(0).notification();
    }
}
