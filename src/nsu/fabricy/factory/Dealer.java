package nsu.fabricy.factory;

import nsu.fabricy.factory.Controllers.StoreReadyController;
import nsu.fabricy.threadpool.ThreadPool;

public class Dealer extends Thread {
    private int num;
    private int time = 3000;
    private ThreadPool tp;
    private  boolean isExecute;
    private  final StoreReadyController storeReadyController;
    @Override
    public void run() {
        int count = 0;
        while(isExecute) {
           // if (count == 30) isExecute = false;
            count++;
            try {
                sleep(time);
                buy();
            } catch (InterruptedException e) {

            }
        }
    }
    public Dealer(ThreadPool tp, int num, StoreReadyController storeReadyController){
        isExecute = true;
        this.tp = tp;
        this.num = num;
        this.storeReadyController = storeReadyController;
    }

    public void buy(){
        storeReadyController.sell(num);
    }
    public void changeTime(int time){
        System.err.println("Время изменилось на:" + time);
        this.time = time;
    }
}
