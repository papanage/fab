package nsu.fabricy.factory.Gui;

import nsu.fabricy.factory.Plant;
import nsu.fabricy.factory.PsModel.Sub;
import nsu.fabricy.factory.frames.Suplier;
import nsu.fabricy.factory.produsers.Producer;
import nsu.fabricy.threadpool.ThreadPool;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Gui extends Sub {
    private Plant plant;
    private JFrame jFrame = new JFrame("Моя маленькая фабрика");
    private JPanel jPanel = new JPanel(new BorderLayout());
    private JPanel jPanelSliders = new JPanel(new BorderLayout());
    private JPanel jPanelSliders2 = new JPanel(new BorderLayout());
    private Font font = new Font("Verdana", Font.PLAIN, 19);
    private JLabel label;


    public  Gui(ThreadPool threadPool,  Plant plant){
        super(threadPool);
        this.plant = plant;
        jPanel.setPreferredSize(new Dimension(200, 400));
        label = createLabelCar();
        label.setFont(font);
        jPanel.add(label, BorderLayout.NORTH);

        jFrame.add(jPanel, BorderLayout.NORTH);

        ButtonGroup group = new ButtonGroup();
        JRadioButton fButton = new JRadioButton("LOG off", false);
        group.add(fButton);
        JRadioButton sButton = new JRadioButton("LOG ON", true);
        group.add(sButton);

        JSlider bslider = createSlider(5000);
        JSlider eslider = createSlider(5000);
        JSlider sslider = createSlider(5000);
        JSlider dslider = createSlider(5000);
        jPanelSliders.add(bslider);
        jPanelSliders.add(eslider, BorderLayout.AFTER_LAST_LINE);
        jPanelSliders2.add(sslider);
        jPanelSliders2.add(dslider, BorderLayout.AFTER_LAST_LINE);
        bslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                plant.changeTime(Plant.pr.BODYP, value);
            }
        });

        eslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                plant.changeTime(Plant.pr.ENGP, value);
            }
        });

        sslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                plant.changeTime(Plant.pr.SUPP, value);
            }
        });

        dslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                plant.changeTime(Plant.pr.DEALC, value);
            }
        });

        jFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                plant.closeFabricy();
                System.exit(0);
            }
        });


        jFrame.add(jPanelSliders,BorderLayout.CENTER);
        jFrame.add(jPanelSliders2,BorderLayout.SOUTH);
        jFrame.setBounds(400,400,700,600);
        //jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setVisible(true);

    }
    @Override
    public void notification(){
        update();
    }
    private void update(){
        label = createLabelCar();
        label.setFont(font);
        jPanel.removeAll();
        jPanel.add(label, BorderLayout.NORTH);
        jFrame.setVisible(true);
    }
    private JLabel createLabelCar(){
        JLabel label  = new JLabel();
        label.setText(
               "<html>Продано машин:   "+plant.getCountCars() +
                "<br> Заданий в очереди: " +plant.getTPSize() +
                "<br> Произведено кузовов:" + plant.getCountGeneral(Plant.pr.BODYP) + " На складе сейчас: " + plant.bodyStore.getRealSize() +
               "<br> Произведено двигателей:" + plant.getCountGeneral(Plant.pr.ENGP) + " На складе сейчас: " + plant.engineStore.getRealSize() +
               "<br> Произведено акссесуаров:" + plant.getCountGeneral(Plant.pr.SUPP) + " На складе сейчас: " + plant.suplierStore.getRealSize() +
                "<br><br><br><br>  1 ползунок - кузовы" +
               "<br>  2 ползунок - двигатели" +
               "<br>  3 ползунок - аксс" +
               "<br>  4 ползунок - дилеры" +
               "</html>");
        return label;
    }
    private JSlider createSlider(int max){
        BoundedRangeModel model = new DefaultBoundedRangeModel(1000, 0, 0, max);
        JSlider slider = new JSlider(model);
        slider.setPaintLabels(true);
        slider.setMajorTickSpacing(500);
        return slider;
    }
}
