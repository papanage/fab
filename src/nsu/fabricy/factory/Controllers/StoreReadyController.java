package nsu.fabricy.factory.Controllers;

import nsu.fabricy.factory.Car;
import nsu.fabricy.factory.Stores.Store;
import nsu.fabricy.factory.TaskCar;
import nsu.fabricy.factory.frames.Body;
import nsu.fabricy.factory.frames.Engine;
import nsu.fabricy.factory.frames.Suplier;
import nsu.fabricy.threadpool.ThreadPool;

import java.util.Date;
import java.util.logging.Logger;

public class StoreReadyController extends Thread {
    private final Store<Car> storeReady;
    private ThreadPool threadPool;
    private final Store<Body> storeBody;
    private final Store<Engine> storeEngine;
    private final Store<Suplier> storeSuplier;
    private Logger logger;
    public StoreReadyController(Store<Car> storeReady,
                                ThreadPool threadPool,
                                Store<Body> storeBody,
                                Store<Engine> storeEngine,
                                Store<Suplier> storeSuplier,
                                Logger logger){
        this.storeReady = storeReady;
        this.threadPool = threadPool;
        this.storeBody = storeBody;
        this.storeEngine = storeEngine;
        this.storeSuplier = storeSuplier;
        this.logger = logger;
    }

    public void sell(int dealerId){
        boolean isSell = false;
        while(!isSell) {
            synchronized (storeReady.frameList) {
                if (storeReady.frameList.isEmpty()) {
                    if (threadPool.getQueueSize() < threadPool.threadCount)
                        for (int u = 0; u < threadPool.threadCount; u++) {
                            threadPool.addTask(new TaskCar(dealerId, storeReady, storeBody, storeEngine, storeSuplier));
                        }
                    try {
                        storeReady.frameList.wait();
                    } catch (InterruptedException e) {
                    }
                }
                System.out.println("продал машину " + dealerId + " " + storeReady.frameList.size());

                if (!storeReady.frameList.isEmpty()){
                    Car car = storeReady.get();
                    logger.info("<"+ new Date()+"> Dealer <"+ dealerId+ "> "+ car.CarLog());
                    threadPool.countDoneInc();
                    isSell = true;
                }

            }
        }
    }
    @Override
    public void run() {
        super.run();
    }

}
