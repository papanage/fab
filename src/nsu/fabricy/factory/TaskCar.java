package nsu.fabricy.factory;

import nsu.fabricy.factory.Stores.Store;
import nsu.fabricy.factory.frames.Body;
import nsu.fabricy.factory.frames.Engine;
import nsu.fabricy.factory.frames.Suplier;
import nsu.fabricy.threadpool.Task;
import nsu.fabricy.threadpool.Worker;

import static java.lang.Thread.sleep;

public class TaskCar implements Task {
    private int dealerNum;
    private final Store<Car> storeReady;
    private final Store<Body> storeBody;
    private final Store<Engine> storeEngine;
    private final Store<Suplier> storeSuplier;
    public TaskCar(int dealerNum,
                   Store<Car> storeReady,
                   Store<Body> storeBody,
                   Store<Engine> storeEngine,
                   Store<Suplier> storeSuplier){
        this.storeReady = storeReady;
        this.dealerNum = dealerNum;
        this.storeBody = storeBody;
        this.storeEngine = storeEngine;
        this.storeSuplier = storeSuplier;
    }
    @Override
    public void performedTask() throws  InterruptedException{
        CreateCar();
    }
    private final void CreateCar(){
        Body body = storeBody.get();
        System.out.println("забрали кузов " +storeBody.getRealSizeSize());
        Engine engine = storeEngine.get();
        System.out.println("забрали движок " +storeEngine.getRealSizeSize());
        Suplier suplier = storeSuplier.get();
        System.out.println("забрали акссесуары " +storeSuplier.getRealSizeSize());
        storeReady.put(new Car(body, engine, suplier));
        System.out.println("сделал машину " +dealerNum);
    }
}
