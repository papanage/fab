package nsu.fabricy.factory.produsers;

import nsu.fabricy.factory.Stores.Store;


public class Producer<T> extends  Thread {
    int count = 0;
    private int time = 1000;
    private Store<T> store;
    private T frame;
    private Class<T> tClass;
    public Producer(Store<T> store, Class<T> tClass){
        this.store = store;
        this.tClass = tClass;
    }

    private void produce(){
        try {
          frame = tClass.getDeclaredConstructor().newInstance();
        }
        catch (Exception e){}

        try {
            sleep(time);
        }
        catch (InterruptedException e){}
        store.put(frame);
        count++;

    }

    @Override
    public void run() {
        while(true){
            produce();
        }
    }
    public void changeTime(int time){
        System.err.println("time change on: "+time);
        this.time = time;
    }

    public Integer getCount(){
        return count;
    }
}
